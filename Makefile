# Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libsuvm.a
LIBNAME_SHARED = libsuvm.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC =\
 src/suvm_device.c\
 src/suvm_primitive.c\
 src/suvm_volume.c\
 src/suvm_volume_at.c\
 src/suvm_volume_intersect_aabb.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libsuvm.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libsuvm.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(EMBREE_VERSION) embree4; then \
	  echo "embree4 $(EMBREE_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -DSUVM_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@EMBREE_VERSION@#$(EMBREE_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    suvm.pc.in > suvm.pc

suvm-local.pc: suvm.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@EMBREE_VERSION@#$(EMBREE_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    suvm.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" suvm.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/suvm.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-uvm" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/suvm.pc"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/suvm.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-uvm/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-uvm/README.md"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libsuvm.o suvm.pc suvm-local.pc
	rm -f ball.vtk box.vtk

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_suvm_device.c\
 src/test_suvm_volume.c\
 src/test_suvm_primitive_intersection.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

SMSH_FOUND = $(PKG_CONFIG) --atleast-version $(SMSH_VERSION) smsh

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SUVM_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags suvm-local.pc)
SUVM_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs suvm-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@if $(SMSH_FOUND); then $(MAKE) src/suvm_voxelize.d; fi; \
	$(MAKE) -fMakefile -f.test \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) \
	$$($(SMSH_FOUND) && echo "-fsrc/suvm_voxelize.d") \
	test_bin

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@{ $(SHELL) make.sh config_test $(TEST_SRC); \
	   if $(SMSH_FOUND); then \
	     $(SHELL) make.sh config_test src/suvm_voxelize.c; fi \
	} > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC) src/suvm_voxelize.c

$(TEST_DEP): config.mk suvm-local.pc
	@$(CC) -std=c89 $(CFLAGS_EXE) $(RSYS_CFLAGS) $(SUVM_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/suvm_voxelize.d: config.mk suvm-local.pc
	@$(CC) -std=c89 $(CFLAGS_EXE) $(RSYS_CFLAGS) $(SUVM_CFLAGS) $(SMSH_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk suvm-local.pc
	$(CC) -std=c89 $(CFLAGS_EXE) $(RSYS_CFLAGS) $(SUVM_CFLAGS) -c $(@:.o=.c) -o $@

src/suvm_voxelize.o: config.mk suvm-local.pc
	$(CC) -std=c89 $(CFLAGS_EXE) $(RSYS_CFLAGS) $(SUVM_CFLAGS) $(SMSH_CFLAGS) -c $(@:.o=.c) -o $@

test_suvm_device \
test_suvm_volume \
test_suvm_primitive_intersection \
: config.mk suvm-local.pc $(LIBNAME)
	$(CC) -std=c89 $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SUVM_LIBS) $(RSYS_LIBS) -lm

suvm_voxelize: config.mk suvm-local.pc $(LIBNAME)
	$(CC) -std=c89 $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SUVM_LIBS) $(RSYS_LIBS) $(SMSH_LIBS) -lm
